const Product = require("../models/Product");
const	User = require("../models/User");


//Create product (Admin)
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product ({

		name:reqBody.name,
		description:reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	})

	return newProduct.save().then((product,error)=>{
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//retrieve ALL products
module.exports.allProducts = ()=>{
	return Product.find().then(result=> {
		return result
	})
}


//retrieve ALL AVAILABLE products
module.exports.getAllAvailable = () => {

	return Product.find({isAvailable:true}).then(result =>{
		return result
	})
};

//retrieve SPECIFIC product by Id
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result 

		//below; to get specific details -
		/*
		let CourseDetails = {
			name: result.name,
			price: result.price
		}
		return CourseDetails  

		*/	
	})
}

//Archive a product by Id (Admin)
module.exports.archiveProduct = (reqParams, reqBody) => {
	let productForArchive = {
		isAvailable:false,
		quantity:0
	}
	//findByIdAndUpdate (document,updatesToBeAplied)
	return Product.findByIdAndUpdate(reqParams.productId, productForArchive).then((product,err)=>{
		if (err){
			return false
		}else {
			return true
		}
	})
}

//Uppdate product info (Admin)
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		quantity: reqBody.quantity
	}
	
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,err)=>{
		if (err){
			return false
		}else {
			return true
		}
	})
}