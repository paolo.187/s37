const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

		
		
		email:{
			type: String,
			required:[true, 'Email is required']
		},

		password:{
			type: String,
			required:[true, 'Password is required']
		},

		isAdmin:{
			type: Boolean,
			default: false
		},

		
		orders:[
			{
				productId: {
					type:String,
					required:[true, 'Product ID is required']
				},

				productName: {
					type: String,
					default: undefined
				},

				quantity: {
					type:Number,
					default: 1
				},

				total: {
					type:Number,
					default: undefined
				},

				

				orderedOn: {
					type: Date,
					default: new Date()
				},

				
			}

		]
})

module.exports = mongoose.model('User',userSchema)


