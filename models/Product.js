const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required:[true, 'Name is required']
	},

	description: {
		type: String,
		required: [true, 'Description is required']
	},

	price: {
		type: Number,
		required: [true, 'Price is required']
	},

	quantity: {
		type: Number,
		default: 0
	},

	isAvailable: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	orders: [
		{
			userId:{
				type:String,
				required:[true, 'User ID is required']
			},

			orderQuantity:{
				type: Number,
				default:undefined
			},

			orderTotal:{
				type:Number,
				default:undefined
			},

			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

})

module.exports = mongoose.model('Product',productSchema)
//////////////////////////////////////////////////////
